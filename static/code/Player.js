
function Player(args){
	if(args === undefined) args = {};
    
	var keys = Object.keys(args);
	var initialize = function(name,init){return keys.indexOf(name) == -1 ? init: args[name]; };
    this._maxX		= initialize('screenWidth', 1000);
	this._position 	= initialize('position', new LV2(100,200));
	this._shape 	= initialize('shape', new LPolygon([new LV2(0,0), new LV2(10, 0), new LV2(5, -20)]));
	this._base 		= initialize('base', this._shape.center());
	this._shape.itransform(LMat3.trans(-this._base.x, -this._base.y));
	this._direction = initialize('direction', new LV2(0,-1));
	this.setDirection(this._direction);
	this._speed 	= initialize('speed', 400);
    this._attachmentInfo = undefined;
	//planet to initialize attachment
	if(keys.indexOf('planet') !== -1) 
		this.forceAttach(args['planet']);
	else
		this._state = 0;
	this._updateShape();
    this._originalArguments = {
        pos : this._position.copy(),
        dir : this._direction.copy()
    };
}

Player.prototype.reset = function(){
    //must reset: _position, _direction, _state
    this._position = this._originalArguments.pos.copy();
    this.setDirection(this._originalArguments.dir.copy());
    this._state = 0;
    this._attachmentInfo = undefined;
    this._updateShape();
};

Player.prototype.setDirection = function(direction){
	this._direction = direction;
	this._angle = this._direction.normal().getAngle();
};

Player.prototype.inBounds = function(){
	var pos = this.getPosition();
	if(pos.x < -20 || pos.x >= this._maxX+20) return false;
	return true;
};

Player.prototype.step = function(time){
	if(this._state == 0){
		var amount = this._speed * (time / 1000);
		this._position.iadd(this._direction.scale(amount));
	}
	this._updateShape();	
};

Player.prototype.getShape = function(){
	return this._computedShape;
};

Player.prototype.getAngle = function(){
	if(this._state == 0) return this._angle;
	else{
		return this._angle + this._attachmentInfo.planet.getAngle();
	}
};

Player.prototype.getPosition = function(){
	if(this._state == 0) return this._position.copy();
	else{
		var p1 = new LV2(0,0);
		var m = this.getTransform();
		p1 = m.multLV2(p1);
		return p1;
	}
};

Player.prototype.getDirection = function(){
	if(this._state == 0) return this._direction.copy();
	else{
		return LV2.fromAngle(this.getAngle()).normal().scale(-1);
	}
};


Player.prototype._updateShape = function(){
	var m = LMat3.trans(this._position.x, this._position.y).mult(LMat3.rotate(this._angle));
	if(this._state == 0) this._computedShape = this._shape.transform(m);
	else{
		m = this._attachmentInfo.planet.getTransform().mult(m);
		this._computedShape = this._shape.transform(m);
	}
};

Player.prototype.getTransform = function(){
	var m = LMat3.trans(this._position.x, this._position.y).mult(LMat3.rotate(this._angle));
	if(this._state == 0) return m;
	else{
		m = this._attachmentInfo.planet.getTransform().mult(m);
		return m;
	}
};

Player.prototype.canBeAttached = function(){
	return this._state == 0;
};

Player.prototype.attach = function(planetsList){
    for(var i = 0; i < planetsList.length && this.canBeAttached(); i++)
		this._attach(planetsList[i]);
};

Player.prototype._attach = function(planet){
	if(this._attachmentInfo !== undefined)
		if(this._attachmentInfo.planet == planet) return;
    var collisions = this._computedShape.collisionInfo(planet.getShape());
    if(collisions.length <= 0) 
		return;
    collisions.sort(function(a,b){return b.distance - a.distance; });
	var collision = collisions[0];
	var info = undefined;
	try{
		info = planet.getAttachmentInfo(collision.point, collision.normal);
	}catch(e){
        return;
	}
	
	//todo, make into animation using Lanimatronics
    
	this.setDirection(info.newDirection);
	this._position = info.newPoint;
	this._attachmentInfo = info;
	this._attachmentInfo.planet = planet;

    this._attachmentInfo.planet.notifyAttach();
    
	this._state = 2;
	this._updateShape();
};

Player.prototype.launch = function(){
	if(this._state !== 2) return;
	
	var p1 = new LV2(0,0);
	var p2 = this._attachmentInfo.newDirection.add(p1);
	
	var m = LMat3.trans(this._position.x, this._position.y).mult(LMat3.rotate(this._angle));
	m = this._attachmentInfo.planet.getTransform().mult(m);
	p1 = m.multLV2(p1);
	p2 = m.multLV2(p2);
	
	var dir = LV2.fromAngle(this._angle + this._attachmentInfo.planet.getAngle()).normal().scale(-1);
	this.setDirection(dir);
	this._position = p1;
	this._state = 0;
    this._attachmentInfo.planet.notifyDetach();
};

Player.prototype.forceAttach = function(planet, edge1, edge2, terpValue){
	if(edge1 === undefined || edge2 === undefined || terpValue === undefined){
		edge1 = 0;
		edge2 = 1;
		terpValue = 0.5;
	}
	var s = planet.getShape();
	edge1 = s.points[edge1];
	edge2 = s.points[edge2];
	var normal = edge2.sub(edge1).normal();
	var point = edge1.interpolateTo(edge2, terpValue);

	var info = planet.getAttachmentInfo(point, normal);

	//todo, make into animation using Lanimatronics

	this.setDirection(info.newDirection);
	this._position = info.newPoint;
	this._attachmentInfo = info;
	this._attachmentInfo.planet = planet;

	this._state = 2;
	this._updateShape();

};

//setup a default player
Player.default = function(position, canvasWidth, speed){
    
    var rocketScalar = 0.8;
    var rocketWidth = rocketScalar * 100;
    
    var rocketShape = '17.5,70;38,75;62,75;82.5,70;62.5,25;60,20;50,15;40,20'.split(';').map(function(x){return x.split(',').map(Number);}).map(function(x){return (new LV2(x[0], x[1])).scale(rocketScalar);});
    var rocketBase = (new LV2(50,75)).scale(rocketScalar);
    var rocketShape = new LPolygon(rocketShape);
    
    
    
    return new Promise(function(res, rej){
        var f = function(image){
            var p = new Player({
                position : position.copy(),
                screenWidth : canvasWidth,
                shape : rocketShape,
                base : rocketBase,
                speed : 600
            });
            p.image = image;
            res(p);
        };
        LCanvas.image('/img/rocket_v2.svg', rocketWidth, rocketWidth, f);
    });
};