/*three types of animation->
parent: Lanimatronics

rotate:
		rpm, clockwise, degrees, timeToLive
translate:
    previous, next, relativeNext, speed (translation-speed), timeToLive
orbit:
    previous, relativeCenter, rpm, degrees, timeToLive


A constructor should have: 
    a type -> rotate, translate or orbit
    specific_data
    an animator
    should be able to:
        compile-itself []
        remove from list
        getForm:
            change number, change point, toggle something
        change-type []
*/

function AnimationConstructor(parent, type){
    this.type = type === undefined ? 'rotate' : type;
    this.parent = parent;
    this.settings = {};
    this.initSpecificData();
}

AnimationConstructor.prototype.toJSON = function(){
    var ret = {
        type : this.type
    };
    var settings = {};
    
    // add settings per type
    
    if(this.type == 'rotate' || this.type == 'orbit'){
        settings.rpm = this.settings.rpm;
        settings.degrees = this.settings.degrees;
        settings.clockwise = this.settings.clockwise;
    }
    if(this.type == 'translate' || this.type == 'orbit') settings.previous = this.settings.previous.toJSON();
    if(this.type == 'translate'){
        settings.next = this.settings.next.toJSON();
        settings.relativeNext = this.settings.relativeNext.toJSON();
        settings.speed = this.settings.speed;
    }
    if(this.type == 'orbit') settings.relativeCenter = this.settings.relativeCenter.toJSON();
    
    settings.timeToLive = this.settings.timeToLive;
    
    //end
    
    
    ret.settings = settings;
    return ret;
};

AnimationConstructor.fromJSON = function(ob, parent){
    var ret = new AnimationConstructor(parent, ob.type);
    var type = ob.type;
    // get settings per type
    if(type == 'rotate' || type == 'orbit'){
        ret.settings.rpm = ob.settings.rpm;
        ret.settings.degrees = ob.settings.degrees;
        ret.settings.clockwise = ob.settings.clockwise;
    }
    if(type == 'translate' || type == 'orbit') ret.settings.previous = LV2.fromJSON(ob.settings.previous);
    if(type == 'translate'){
        ret.settings.next = LV2.fromJSON(ob.settings.next);
        ret.settings.relativeNext = LV2.fromJSON(ob.settings.relativeNext);
        ret.settings.speed = ob.settings.speed;
    }
    if(type == 'orbit') ret.settings.relativeCenter = LV2.fromJSON(ob.settings.relativeCenter);
    
    ret.settings.timeToLive = ob.settings.timeToLive;
    
    //end
    return ret;
};


AnimationConstructor.prototype.copy = function(){
    var other = this;
    var ret = new AnimationConstructor(this.parent);
    var set = function(name){
        ret.settings[name] = other.settings[name];
    };
    var setC = function(name){
        ret.settings[name] = other.settings[name].copy();
    };
    ret.type = this.type;
    if(this.type == 'rotate' || this.type == 'orbit'){
        set('rpm');
        set('degrees');
        set('clockwise');
    }
        
    if(this.type == 'translate' || this.type == 'orbit')
        setC('previous');
    if(this.type == 'translate'){
        setC('next');
        setC('relativeNext');
        set('speed');
    }
    if(this.type == 'orbit')
        setC('relativeCenter');
    set('timeToLive');
    return ret;
};

AnimationConstructor.prototype.defVal = function(name, v){
    if(this.settings[name] === undefined)
        this.settings[name] = v;
};

AnimationConstructor.prototype.defValC = function(name, v){
    if(this.settings[name] === undefined)
        this.settings[name] = v.copy();
};

AnimationConstructor.prototype.initSpecificData=function(){
    if(this.type == 'rotate' || this.type == 'orbit'){
        this.defVal('rpm', 1);
        this.defVal('degrees', 360);
        this.defVal('clockwise', false);
    }
        
    if(this.type == 'translate' || this.type == 'orbit')
        this.defValC('previous', new LV2(0,0));
    if(this.type == 'translate'){
        this.defVal('next', new LV2(0,0));
        this.defValC('relativeNext', new LV2(0,0));
        this.defVal('speed', 1);
    }
    if(this.type == 'orbit')
        this.defValC('relativeCenter', new LV2(0,0));
    this.defVal('timeToLive', -3);
};
AnimationConstructor.prototype.compile=function(){
    return new Lanimatronics[this.type](this.settings);
};
AnimationConstructor.prototype.changeType=function(newType){
    if(newType != this.type) {
        this.type = newType;
        this.initSpecificData();
        return true;
    }
    return false;
};
AnimationConstructor.prototype.setInitialPosition = function(pos){
    if(this.type == 'translate' || this.type == 'orbit')
        this.settings.previous = pos.copy();
};
AnimationConstructor.prototype.getForm = function(){
    
    var ret = $('<div class="animeDiv"></div>');
    
    /*button(parent, name, this, func)
    input(parent, msg, name, this, func)
    toggle(parent, msg, initialValue (true or false), this, func)
    pointInput(parent, name, currentPoint, this, thisPlanet, funcInput[, funcClick])
    colorInput(parent, msg, currentColor, this, func)
    select(parent, name, options, default, this, func)*/
    var button = LevelCreator.button;
    var input = LevelCreator.input;
    var toggle= LevelCreator.toggle;
    var point = LevelCreator.pointInput;
    var select = LevelCreator.select;
    
    button(ret, 'dec', this, function(){
        var index1 = this.getIndex();
        var index2 = index1-1 < 0 ? this.parent.animationList.length-1 : index1-1; 
        var temp = this.parent.animationList[index1];
        this.parent.animationList[index1] = this.parent.animationList[index2];
        this.parent.animationList[index2] = temp;
        this.parent.compilePlanet();
        this.parent.lc.updateGui();
    });
    button(ret, 'inc', this, function(){
        var index1 = this.getIndex();
        var index2 = index1+1 >= this.parent.animationList.length ? 0 : index1+1; 
        var temp = this.parent.animationList[index1];
        this.parent.animationList[index1] = this.parent.animationList[index2];
        this.parent.animationList[index2] = temp;
        this.parent.compilePlanet();
        this.parent.lc.updateGui();
    });
    button(ret, 'delete', this, function(){
        var me = this;
        this.parent.animationList = this.parent.animationList.filter(function(x){ return x!=me; });
        this.parent.compilePlanet();
        this.parent.lc.updateGui();
    });
    button(ret, 'clone', this, function(){
        this.parent.animationList.push(this.copy());
        this.parent.compilePlanet();
        this.parent.lc.updateGui();
    });
    
    select(ret, 'set type: ', ['rotate','translate','orbit'], this.type, this, function(newType){
        if(this.changeType(newType)){
            this.parent.compilePlanet();
            this.parent.lc.updateGui();
        }
    });
    
    if(this.type == 'rotate' || this.type == 'orbit'){
        input(ret, 'rpm: ', this.settings.rpm, this, function(newRPM){
            this.settings.rpm = Number(newRPM);
            this.parent.compilePlanet();
        });
        input(ret, 'degrees: ', this.settings.degrees, this, function(newD){
            this.settings.degrees = Number(newD);
            this.parent.compilePlanet();
        });
    }
    if(this.type == 'rotate' || this.type == 'orbit'){
        toggle(ret, 'clockwise: ', this.settings.clockwise, this, function(cw){
            this.settings.clockwise = cw;
            this.parent.compilePlanet();
        });
    }
    
    if(this.type == 'translate'){
        //current : set from input and from a button click
        point(ret, 'next', this.settings.next.copy(), this, this.parent, function(newPoint){
            this.settings.next = newPoint.copy();
        });
        point(ret, 'relative-next', this.settings.relativeNext.copy(), this, this.parent, function(newPoint){
            this.settings.relativeNext = newPoint.copy();
        });
        input(ret, 'speed: ', this.settings.speed+'', this, function(newSpeed){
            this.settings.speed = newSpeed;
            this.parent.compilePlanet();
        });
    }
    
    if(this.type == 'orbit'){
        point(ret, 'relative-center', this.settings.relativeCenter.copy(), this, this.parent, function(newPoint){
            this.settings.relativeCenter = newPoint.copy();
        }, function(newPoint){
            this.settings.relativeCenter = newPoint.sub(this.parent.position);
            this.parent.lc.updateGui();
        });
    }
    
    toggle(ret, 'infinite-repeat: ', this.settings.timeToLive==-3, this, function(isInf){
        if(!isInf) this.settings.timeToLive = 1;
        else this.settings.timeToLive = -3;
        this.parent.compilePlanet();
        this.parent.lc.updateGui();
    });
    if(this.settings.timeToLive != -3){
        input(ret, 'repeat-times: ', this.settings.timeToLive+'', this, function(newInp){
            var ttl = Math.round(Number(newInp));
            if(ttl <= -1) alert('cannot have ttl less than 0');
            else{
                this.settings.timeToLive = ttl;
                this.parent.compilePlanet();
            }
        });
    }
    return ret;
};

AnimationConstructor.prototype.getIndex = function(){
    var i = 0;
    for(var i = 0; i < this.parent.animationList.length; i++)
        if(this.parent.animationList[i] == this) return i;
    return -1;
};



