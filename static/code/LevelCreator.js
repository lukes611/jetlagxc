/*
LC -> level creator has:
    int counter
    int state
    [] allPlanets
    [] death planets
    [] healthy planets
    [] drawfunction
    int yfocus
    list of mouse listners
    a click function
    getPlanets()
    removePlanet()
    addPlanet()
    compilePlanets()

*/

function LevelCreator(){
    var me = this;
    this.editorMode = 'edit';
    this.name = 'level x';
    this.guiState = 'planets';
    this.focusPlanet = undefined;
    this._id = 0;
    this.planets = [];
    this.yFocus = 0;
    this.mouseListeners = [];
    this.device = new LDevice();
    this.canvas = new LCanvas(480, 800);
    
    //add top buttons:
    this.editModeButton = $('#modeFunctionButton');
    
    this.debugPoint = new LV2(0,0);
    
    this.init();
}

LevelCreator.prototype.save = function(){
    $.post('/saveLevel', {name : this.name, level : JSON.stringify(this.toJSON())}, function(ret){
        if(ret.error){
            alert('failed to save');
        }
    });
};

LevelCreator.prototype.toJSON = function(){
    var ret = {
        name : this.name,
        _id : this._id,
        yFocus : this.yFocus,
        planets : this.planets.map(function(x){return x.toJSON();})
    };
    return ret;
};

LevelCreator.prototype.fromJSON = function(ob){
    this.yFocus = ob.yFocus;
    this._id = ob._id;
    this.name = ob.name;
    var me = this;
    this.planets = ob.planets.map(function(x){return PlanetConstructor.fromJSON(x, me);});
    this.nameChanger.val(this.name);
    this.setGuiState('planets');
};

LevelCreator.prototype.init = function(){
    var me = this;
    this.device.setRepaintTimeout(33);
    this.canvas.canvas.id = "screen";
    this.nameChanger = $('#nameInputDiv01');
    this.nameChanger.change(function(){
        me.name = me.nameChanger.val();
    });
    
    $('#canvasBox').append(this.canvas.canvas);
    
    //setup player
    Player.default(this.canvas.getCenter().add(new LV2(-50,-400)), this.canvas.w, 600).then((function(player){
        this.player = player;
        
        this.device.repaint(function draw(){
            me.paint();
            me.device.repaint(draw);
        });
        this.initMouse();
        this.updateGui();
        setInterval((function(){
            if(this.editorMode == 'debug')
                for(var i = 0; i < 5; i++)
                  this.step(33/5);
        }).bind(this), 33);

    }).bind(this));
    
    

};
//add planets: addPlanet() : creates new planet, addPlanet(planet) : adds planet
LevelCreator.prototype.addPlanet = function(input){
    if(input === undefined) input = new PlanetConstructor(this);
    this.planets.push(input);
    this.guiState = 'planets';
    this.updateGui();
};
//returns {index: x, planey: p}, else undefined
LevelCreator.prototype.findPlanetByName = function(name){
    for(var i = 0; i < this.planets.length; i++){
        if(this.planets[i].name == name) return {
            index : i,
            planet : this.planets[i]
        };
    }
};
LevelCreator.prototype.newPlanetName = function(){
    return 'planet.' + this._id++;
};
LevelCreator.prototype.setGuiState = function(newGuiState, planet){
    this.guiState = newGuiState;
    this.focusPlanet = planet;
    this.updateGui();
};
LevelCreator.prototype.resetMouseListeners = function(){
    this.mouseListeners = [];
};
LevelCreator.prototype.addMouseListener = function(mlo){
    this.mouseListeners.push(mlo);
};


//change mode from edit and debug modes, if mode given as paremeter: change to this else
LevelCreator.prototype.changeMode = function(newMode){
    if(newMode === undefined) newMode = this.editorMode == 'edit' ? 'debug' : 'edit';
    if(newMode == 'debug')
        this.editModeButton.html('debug mode').css('background-color', 'red');
    else
        this.editModeButton.html('edit mode').css('background-color', 'blue');
    this.editorMode = newMode;
    this.player.reset();
    this.compile(); //compile all planets
    
    if(newMode == 'debug'){ //attach player to any starter if required
        for(var i = 0; i < this.planets.length; i++){
            if(this.planets[i].planetType == 'starter'){
                this.player.forceAttach(this.planets[i].planet);
                break;
            }
        }
    }
    
};

//paint the canvas
LevelCreator.prototype.paint = function(){
    var c = this.canvas;
    
    //clear screen
    c.resetTransform();
    c.setFill('black');
    c.rectf(0,0,c.w, c.h);
    
    //write the debugPoint location
    c.context.font = '30px Arial';
    c.setFill('white');
    c.text(10, 30, this.debugPoint.round().toString());
    
    //get the player location
    var playerLocation = this.player.getPosition();
    
    //follow the editor or the player location
    if(this.editorMode == 'edit') c.context.translate(0, -this.yFocus + c.h/2);
    else c.context.translate(0, -playerLocation.y + c.h/2);
    
    
    //draw debug location
    c.setStroke('white');
    if(this.editorMode == 'edit') c.circle(this.debugPoint.x, this.debugPoint.y, 10)
    
    
    //draw player:
    var base = this.player._base.scale(-1);
    c.context.save();	
    c.context.translate(playerLocation.x, playerLocation.y);
    c.context.rotate(this.player.getAngle() / 57.3);
    c.context.translate(base.x, base.y);
    c.dimage(0, 0, this.player.image);
    c.context.restore();
	//draw the planets
    for(var i = 0; i < this.planets.length; i++){
        var p = this.planets[i].planet;
        this.drawPlanet(p);
        //c.setFill(p.color);
        //c.polyf(p.getShape().points);
    }
};

LevelCreator.prototype.drawPlanet = function(p){
    var t = p._type;
    if(t == 'stick' || t == 'starter' || t == 'ender'){
        this.canvas.setFill(p.color);
        this.canvas.polyf(p.getShape().points);
    }else if(t == 'death'){
        this.canvas.setStroke(p.color);
        this.canvas.lineWidth = 35;
        this.canvas.poly(p.getShape().points);
    }else if(t == 'timer'){
        this.canvas.setFill(p.color);
        this.canvas.polyf(p.getShape().points);
        var pos = p.getPosition();
        this.canvas.setFill('black');
        this.canvas.text(pos.x-10, pos.y+5, p.getTime())
    }
        
};

//mouse functions
LevelCreator.prototype.mouseClick = function(point){
    if(this.editorMode == 'debug') this.player.launch();
    this.mouseListeners.forEach(function(x){
        x.i -= 1;
        x.f(point);
    });
    this.mouseListeners = this.mouseListeners.filter(function(x){ return x.i >= 1; });
};
LevelCreator.prototype.mouseMove = function(point, previous, difference){
    if(this.editorMode == 'edit') this.yFocus -= difference.y;
};
LevelCreator.prototype.mouseEnd = function(point, start){};

//update the rhs gui
LevelCreator.prototype.updateGui = function(){
    var output = $('#controlsBucket');
    output.empty();
    if(this.guiState == 'planets'){ //if user wants to manipulate the list of planets
        for(var i = 0; i < this.planets.length; i++)
            output.append(this.planets[i].getWidget());
    }else if(this.guiState == 'planet'){ //if the user wants to manipulate a specific planet
        if (this.focusPlanet !== undefined) output.append(this.focusPlanet.getForm());    
    }
};

LevelCreator.prototype.step = function(time){
    var list = [];
    for(var i = 0; i < this.planets.length; i++) list.push(this.planets[i].planet);
    this.player.attach(list);
    for(var i = 0; i < list.length; i++) list[i].step(time);
    this.player.step(time);
	if(!this.player.inBounds()){
        this.player.reset();
        this.changeMode('edit');
    }
};

LevelCreator.prototype.compile = function(){
    for(var i = 0; i < this.planets.length; i++)
        this.planets[i].compilePlanet();
};



