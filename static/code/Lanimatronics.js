/*
adjustable features:
	rotate:
		rpm, clockwise, degrees, timeToLive
	translate:
		previous, next, relativeNext, speed (translation-speed), timeToLive
	orbit:
		previous, relativeCenter, rpm, degrees, clockwise, timeToLive : add clockwise to things
*/
function Lanimatronics(){
	this.timeToLive = -3;
}
Lanimatronics.prototype.isInfinite = function(){ return this.timeToLive == -3; };
Lanimatronics.prototype.fillInParameters = function(params){
	for(var e in params)
		this[e] = params[e];
};
Lanimatronics.prototype.alive = function(){
	if(this.isInfinite()) return true;
	else if(this.timeToLive <= 0) return false;
	return true;
};

Lanimatronics.prototype.reduceLife = function(){
	if(!this.isInfinite() && this.timeToLive > 0) this.timeToLive -= 1;
}

Lanimatronics.prototype.stepAngle = function(time, angle){
	if(this.finished()) return angle;
	var am = this.rpms * time;
	var totalAmount = am + this.count;
	if(totalAmount > this.numDegrees)
		am = this.numDegrees - this.count;
	this.count += am;
	if(this.clockwise){
		angle += am;
		angle %= 360;
	}else{
		angle -= am;
		if(angle < 0) angle = 360 + angle;
	}
	return angle;
};

Lanimatronics.rotate = function(params){
    this.__proto__.__proto__ = Lanimatronics.prototype;
    Lanimatronics.call(this);
	this.rpm = 1;
	this.clockwise = true;
	this.degrees = 0;
	this.count = 0;
	this.fillInParameters(params);
	this._type = 'rotate';
	this.rpms = this.rpm * 60 / 1000;
};
//returns true if finished animation
Lanimatronics.rotate.prototype.step = function(time, angle){
	return this.stepAngle(time, angle);
};
Lanimatronics.rotate.prototype.finished = function(){
	return this.count >= this.degrees;
};

Lanimatronics.rotate.prototype.reset = function(){
	this.count = 0;
	this.reduceLife();
};

Lanimatronics.rotate.prototype.toJSON = function(){
    /*
        rpm, clockwise, degrees, count, _type, timeToLive
    */
    return {
        rpm : this.rpm,
        clockwise : this.clockwise,
        degrees : this.degrees,
        _type : this._type,
        timeToLive : this.timeToLive
    };
};

Lanimatronics.rotate.fromJSON = function(ob){
    return new Lanimatronics.rotate(ob);
};


Lanimatronics.translate = function(params){
	this.__proto__.__proto__ = Lanimatronics.prototype;
    Lanimatronics.call(this);
	this.speed = 1;
	this.previous = new LV2(0,0);
	this.next = new LV2(0,0);
	this.relativeNext = new LV2(0,0);
    this.fillInParameters(params);
	var ttl = this.timeToLive;
	this.reset(this.previous);
	this.timeToLive = ttl;
	this._type = 'translate';
};

Lanimatronics.translate.prototype.toJSON = function(){
    //previous, next, relativeNext, speed
    return {
        previous : this.previous.toJSON(),
        next : this.next.toJSON(),
        relativeNext : this.relativeNext.toJSON(),
        speed : this.speed,
        timeToLive : this.timeToLive,
        _type : this._type
    };
};

Lanimatronics.translate.fromJSON = function(ob){
    ob.previous = LV2.fromJSON(ob.previous);
    ob.next = LV2.fromJSON(ob.next);
    ob.relativeNext = LV2.fromJSON(ob.relativeNext);
    return new Lanimatronics.translate(ob);
};

//returns true if finished animation
Lanimatronics.translate.prototype.step = function(time){
	if(!this.finished()){
		var am = Math.min(this.current.dist(this.next), this.speed);
		var impulse = this.dir.scale(am);
		this.current.iadd(impulse);
	}
	return this.current;
};


Lanimatronics.translate.prototype.finished = function(){
	return this.current.dist(this.next) < 0.05;
};

Lanimatronics.translate.prototype.reset = function(currentPosition){
	this.previous = currentPosition.copy();
	this.current = currentPosition;
	this.next.iadd(this.relativeNext);
	this.dir = this.next.sub(this.previous);
	this.dir.iunit();
	this.reduceLife();
};

Lanimatronics.orbit = function(params){
	this.__proto__.__proto__ = Lanimatronics.prototype;
    Lanimatronics.call(this);
	this.rpm = 1;
	this.clockwise = true;
	this.degrees = 0;
	this.relativeCenter = new LV2(0,-50);
	this.previous = new LV2(0,0);
    this.current = new LV2(0,0);
	this.count = 0;
	this.fillInParameters(params);
	this.rpms = this.rpm * 60 / 1000;
	var ttl = this.timeToLive;
	this.reset(this.previous);
	this.timeToLive = ttl;
	this._type = 'orbit';
}

Lanimatronics.orbit.prototype.step = function(time){
	if(this.finished()) return this.current;
	this.angle = this.stepAngle(time, this.angle);
	var m = this.mat();
	this.current = m.multLV2(this.previous);
	return this.current;
};

Lanimatronics.orbit.prototype.finished = function(){
	return this.count >= this.degrees;
};

Lanimatronics.orbit.prototype.reset = function(currentPosition){
	this.count = 0;
	this.current = currentPosition.copy();
	this.previous = currentPosition.copy();
	this.center = this.relativeCenter.add(this.current);
	this.angle = 0;
	this.reduceLife();
};

Lanimatronics.orbit.prototype.toJSON = function(){
    //previous, relativeCenter, rpm, degrees
    return {
        previous : this.previous.toJSON(),
        relativeCenter : this.relativeCenter.toJSON(),
        rpm : this.rpm,
        degrees : this.degrees,
        clockwise : this.clockwise,
        timeToLive : this.timeToLive,
        _type : this._type
    };
};

Lanimatronics.orbit.fromJSON = function(ob){
    ob.previous = LV2.fromJSON(ob.previous);
    ob.relativeCenter = LV2.fromJSON(ob.relativeCenter);
    return new Lanimatronics.orbit(ob);
};

Lanimatronics.orbit.prototype.mat = function(){
	return LMat3.trans(this.center.x, this.center.y).mult(LMat3.rotate(this.angle).mult(LMat3.trans(-this.center.x, -this.center.y)));
};


Lanimatronics.fromJSON = function(ob){
    if(ob._type == 'rotate') return Lanimatronics.rotate.fromJSON(ob);
    else if(ob._type == 'orbit') return Lanimatronics.orbit.fromJSON(ob);
    return Lanimatronics.translate.fromJSON(ob);
};

