function LMenuButton(name, cb){
    this.name = name;
    this.cb = cb;
    this.$ = $('<div style="float:left;"></div>');
    var btn = $('<button style="width:150px;">'+this.name+'</button>');
    if(cb !== undefined) btn.click(cb.bind(btn));
    this._$ = $('<div style="position:absolute;display:none" ></div>');
    this.$.append(btn);
    this.$.append(this._$);
    
    this.$.mouseover(this.openMenu.bind(this));
    this.$.mouseout(this.closeMenu.bind(this));
    
    this.buttonList = [btn];
};

LMenuButton.prototype.append = function(name, func){
    var btn = $('<button style="width:150px;">'+name+'</button>');
    if(func !== undefined)
        btn.click(func.bind(btn));
    var outer = $('<div></div>');
    outer.append(btn);
    this._$.append(outer);
    this.buttonList.push(btn);
};

LMenuButton.prototype.openMenu = function(){
    this._$.css('display', 'block');
};

LMenuButton.prototype.closeMenu = function(){
    this._$.css('display', 'none');
};

