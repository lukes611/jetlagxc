//PlanetX is a namespace to hold different types of planets
function PlanetX(){
    
};

//a Stick Planet is a regular one in which the user sticks to it
PlanetX.stick = function(args){
    this.__proto__.__proto__ = Planet.prototype;
    Planet.call(this, args);
    this._type = 'stick';
};

PlanetX.stick.prototype.toJSON = function(){
    var ret = {
        args: Planet.prototype.toJSON.call(this)
    };
    ret._type = 'stick';
    return ret;
};

PlanetX.stick.fromJSON = function(ob){
    var ret = { _type : 'stick' };
    ret = Planet.fromJSON(ob.args);
    ret._type = 'stick';
    ret.__proto__ = PlanetX.stick.prototype;
    ret.__proto__.__proto__ = Planet.prototype;
    return ret;
};


PlanetX.death = function(args){
    this.__proto__.__proto__ = Planet.prototype;
    Planet.call(this, args);
    this._type = 'death';
};


PlanetX.death.prototype.notifyAttach = function(){
    console.log('player should die!');
};

PlanetX.death.prototype.toJSON = function(){
    var ret = {
        args: Planet.prototype.toJSON.call(this)
    };
    ret._type = 'death';
    return ret;
};

PlanetX.death.fromJSON = function(ob){
    var ret = { _type : 'death' };
    ret = Planet.fromJSON(ob.args);
    ret._type = 'death';
    ret.__proto__ = PlanetX.death.prototype;
    ret.__proto__.__proto__ = Planet.prototype;
    return ret;
};


PlanetX.starter = function(args){
    this.__proto__.__proto__ = Planet.prototype;
    Planet.call(this, args);
    this._type = 'starter';
};

PlanetX.starter.prototype.toJSON = function(){
    var ret = {
        args: Planet.prototype.toJSON.call(this)
    };
    ret._type = 'starter';
    return ret;
};

PlanetX.starter.fromJSON = function(ob){
    var ret = { _type : 'starter' };
    ret = Planet.fromJSON(ob.args);
    ret._type = 'starter';
    ret.__proto__ = PlanetX.starter.prototype;
    ret.__proto__.__proto__ = Planet.prototype;
    return ret;
};


PlanetX.ender = function(args){
    this.__proto__.__proto__ = Planet.prototype;
    Planet.call(this, args);
    this._type = 'ender';
};


PlanetX.ender.prototype.notifyAttach = function(){
    console.log('congrats finished game');
};


PlanetX.ender.prototype.toJSON = function(){
    var ret = {
        args: Planet.prototype.toJSON.call(this)
    };
    ret._type = 'ender';
    return ret;
};

PlanetX.ender.fromJSON = function(ob){
    var ret = { _type : 'ender' };
    ret = Planet.fromJSON(ob.args);
    ret._type = 'ender';
    ret.__proto__ = PlanetX.ender.prototype;
    ret.__proto__.__proto__ = Planet.prototype;
    return ret;
};


/*

timer ideas: record the time when attaching, also timer has a time explicitly
default time is 5, called countDown

*/

PlanetX.timer = function(args){
    if(args === undefined) args = {};
    this.__proto__.__proto__ = Planet.prototype;
    Planet.call(this, args);
    this._type = 'timer';
    this.countDown = args.countDown === undefined ? 5 : args.countDown;
    this.time = this.countDown;
    this.isAttached = false;
    this.isDead = false;
};

PlanetX.timer.prototype.toJSON = function(){
    var ret = {
        args: Planet.prototype.toJSON.call(this)
    };
    ret._type = 'timer';
    ret.countDown = this.countDown;
    return ret;
};

PlanetX.timer.fromJSON = function(ob){
    var ret = { _type : 'timer' };
    ret = Planet.fromJSON(ob.args);
    ret._type = 'timer';
    //setup timer vars
    this.countDown = ob.countDown;
    this.time = this.countDown;
    this.isAttached = false;
    this.isDead = false;
    //end
    ret.__proto__ = PlanetX.timer.prototype;
    ret.__proto__.__proto__ = Planet.prototype;
    return ret;
};


PlanetX.timer.prototype.getTime = function(){
    return Math.round(this.time);
};


PlanetX.timer.prototype.notifyAttach = function(){
    this.isAttached = true;
};

PlanetX.timer.prototype.notifyDetach = function(){
    if(!this.isDead){
        this.isAttached = false;
        this.time = this.countDown;
    }
};

PlanetX.timer.prototype.notifyStep = function(time){
    if(this.isAttached){
        this.time -= time / 1000.0;
        if(this.time <= 0){
            this.time = 0;
            this.isDead = true;
            console.log('player should die!');
        }    
    }
    
};

PlanetX.fromJSON = function(ob){
    return PlanetX[ob._type].fromJSON(ob);
};




