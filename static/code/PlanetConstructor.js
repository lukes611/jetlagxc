//A constructor for planets

function PlanetConstructor(lc){
    this.lc = lc;
    this.name = this.lc === undefined ? '_' : this.lc.newPlanetName();
    this.position = new LV2(0,0);
    this.base = new LV2(0,0);
    this.shapeType = 'circle';
    this.planetType = 'stick';
    this.countDown = 5;
    this.radius = 30;
    this.angle = 0;
    this.scalar = 1;
    this.color = '#5612d7';
    //this.planet = new PlanetX[this.planetType]();
    this.shape = Planet[this.shapeType](this.radius);
    this.animationList = [];
    this.compilePlanet();
}
//get a deep copy of this
PlanetConstructor.prototype.copy = function(){
    var ret = new PlanetConstructor(this.lc);
    ret.name += ' (copy of ' + this.name + ')';
    ret.position = this.position.copy();
    ret.base = this.base.copy();
    ret.shapeType = this.shapeType;
    ret.planetType = this.planetType;
    ret.countDown = this.countDown;
    ret.radius = this.radius;
    ret.angle = this.angle;
    ret.scalar = this.scalar;
    ret.color = this.color;
    ret.shape = this.shape.copy();
    ret.animationList = this.animationList.map(function(x){
        var rv = x.copy();
        rv.parent = ret;
        return rv; 
    });
    
    ret.compilePlanet();
    return ret;
};

PlanetConstructor.prototype.toJSON = function(){
    var ret = {
        name : this.name,
        position : this.position.toJSON(),
        base : this.base.toJSON(),
        shapeType : this.shapeType,
        planetType : this.planetType,
        countDown : this.countDown,
        radius : this.radius,
        angle : this.angle,
        scalar : this.scalar,
        color : this.color,
        shape : this.shape.toJSON(),
        animationList : this.animationList.map(function(x){return x.toJSON();})
    };
    return ret;
};

PlanetConstructor.fromJSON = function(ob, lc){
    var ret = new PlanetConstructor(lc);
    ret.lc = lc;
    ret.name = ob.name;
    ret.position = LV2.fromJSON(ob.position);
    ret.base = LV2.fromJSON(ob.base);
    ret.shapeType = ob.shapeType;
    ret.planetType = ob.planetType;
    ret.countDown = ob.countDown;
    ret.radius = ob.radius;
    ret.angle = ob.angle;
    ret.scalar = ob.scalar;
    ret.color = ob.color;
    ret.shape = LPolygon.fromJSON(ob.shape);
    ret.animationList = ob.animationList.map(function(x){
        return AnimationConstructor.fromJSON(x, ret); 
    });
    
    ret.compilePlanet();
    return ret;
};

//get the planet primary controls
PlanetConstructor.prototype.getWidget = function(){
    var newElement = $('<div class="PlanetSelect"></div>');
    newElement.css('background-color', this.color);
    
    LevelCreator.button(newElement, 'X', this, function(){
        var info = this.lc.findPlanetByName(this.name);
        if(info !== undefined) this.lc.planets.splice(info.index, 1);
        this.lc.updateGui();
    }).prop('class', 'PlanetSelectX');
    
    newElement.append($('<div class="PlanetSelectName">'+this.name+'</div>'));
    LevelCreator.button(newElement, 'edit', this, function(){
        this.lc.setGuiState('planet', this);
    }).prop('class', 'PlanetSelectButton');
    LevelCreator.button(newElement, 'copy', this, function(){
        this.lc.addPlanet(this.copy());
    }).prop('class', 'PlanetSelectButton');
    
    return newElement;
};
PlanetConstructor.prototype.getForm = function(){
    var ret = $('<div class="PlanetForm"></div>');
    /*
    useful function prototypes:
    
    button(parent, name, this, func)
    input(parent, msg, name, this, func)
    toggle(parent, msg, initialValue (true or false), this, func)
    pointInput(parent, name, currentPoint, this, thisPlanet, funcInput[, funcClick])
    colorInput(parent, msg, currentColor, this, func)
    select(parent, name, options, default, this, func)
    */
    var button = LevelCreator.button;
    var input = LevelCreator.input;
    var toggle = LevelCreator.toggle;
    var color = LevelCreator.colorInput;
    var point = LevelCreator.pointInput;
    var select = LevelCreator.select;
    
    var primarySection = $('<div class="PlanetFormPrimary"></div>');
    var primarySection2 = $('<div class="PlanetFormPrimary"></div>');
    
    var btnSection = $('<div class="PlanetFormBtn"></div>');
    
    
    button(btnSection, 'return', this, function(){
        this.lc.resetMouseListeners();
        this.lc.setGuiState('planets');
    });
    
    button(btnSection, 'add animation', this, function(){
        this.animationList.push(new AnimationConstructor(this));
        this.compilePlanet();
        this.lc.updateGui();
    });
    
    
    
    input(primarySection, 'name: ', this.name, this, function(newName){
        this.name = newName;
    });
    
    
    
    point(primarySection, 'position', this.position.copy(), this, this, function(p){
        this.position = p.copy();
        this.compilePlanet();
    });
    color(primarySection, 'color: ', this.color, this, function(newColor){
        this.color = newColor;
        this.compilePlanet();
    });
    
    select(primarySection, 'planet type: ', 'stick death starter ender timer'.split(' '), this.planetType, this, function(newType){
        this.planetType = newType;
        this.lc.updateGui();
        this.compilePlanet();
    });
    
    if(this.planetType == 'timer'){
        input(primarySection, 'count down', this.countDown, this, function(newCountDown){
            this.countDown = newCountDown;
            this.compilePlanet();
        });
    }
    
    
    select(primarySection, 'shape type: ', ['random', 'triangle', 'circle', 'square', 'hexigon', 'octagon'], this.shapeType, this, function(newType){
        this.shapeType = newType;
        this.shape = Planet[this.shapeType](this.radius);
        this.lc.updateGui();
        this.compilePlanet();
    });
    
    if(this.shapeType == 'circle'){
        input(primarySection, 'radius: ', this.radius+'', this, function(newValue){
            this.radius = Number(newValue);
            this.shape = Planet[this.shapeType](this.radius);
            this.compilePlanet();
            
        });
    }else{
        var inp = this.shape.points.reduce(function(p,oc){
            var c = oc.copy();
            c.iscale(100); c.iround(); c.idiv(100);
            return p + ';' + c.x + ',' + c.y;
        }, '').slice(1);
        var shi = input(primarySection2, 'shape: ', inp, this, function(newInp){
            newInp = newInp.split(';').map(function(str){
                var ns = str.split(',').map(Number);
                return new LV2(ns[0], ns[1]);
            });
            this.shape = new LPolygon(newInp);
            this.compilePlanet();
        });
        
        
        button(shi[0], 'round shape points', this, function(){
            this.shape.points.forEach(function(x){x.iround();});
            this.compilePlanet();
            this.lc.updateGui();
        });
        
        input(primarySection2, 'base: ', this.base+'', this, function(newPos){
            this.base = LV2.fromString(newPost);
            this.compilePlanet();
        });
        input(primarySection2, 'scale: ', this.scalar, this, function(newInp){
            this.scalar = Number(newInp);
            this.compilePlanet();
        });
        
    }
    
    input(primarySection2, 'angle: ', this.angle+'', this, function(newAngle){
        this.angle = Number(newAngle);
        this.compilePlanet();
    });
    
    primarySection2.append(btnSection);
    
    ret.append(primarySection);
    ret.append(primarySection2);
    
    this.animationList.forEach(function(anime){
        ret.append(anime.getForm());
    });
    
    
    
    return ret;
};

//compiles the planet from the settings
PlanetConstructor.prototype.compilePlanet = function(){
    var matrix = LMat3.scale(this.scalar).mult(LMat3.trans(-this.base.x, -this.base.y));
    var settings = {
        position : this.position,
        shape : this.shape.transform(matrix),
        angle : this.angle,
        color : this.color,
        countDown : this.countDown
    };
    this.planet = new PlanetX[this.planetType](settings);
    for(var i = 0; i < this.animationList.length; i++){
        this.animationList[i].setInitialPosition(this.position);
        this.planet.addAnimation(this.animationList[i].compile());
    }
};

