function LDB(){
}

LDB.prototype.isLoggedIn = function(){
    return this.local('un') !== undefined;
};

//get the username of the currently logged in user, else undefined
LDB.prototype.un = function(){ return this.local('un'); };

//check if logged in
LDB.prototype.init = function(){
    var me = this;
    return new Promise(function(res, rej){
        if(me.isLoggedIn()) //if i am logged in: resolve right away
            res(true);
        else{
            $.get('/li', function(user){
                if(user.error){
                    res(false); //still resolve but there is no user
                }else{
                    me.local('un', user.un);
                    res(true);
                }
            });
        }
    });
};

//log in
LDB.prototype.logIn = function(un, pw){
    var me = this;
    return new Promise(function(res, rej){
        $.post('/li', {un : un, pw: pw}, function(user){
            if(!user.error) me.local('un',user.un);
            res(!user.error);
        });
    });
};

//create new account
LDB.prototype.newAccount = function(un, pw){
    var me = this;
    return new Promise(function(res, rej){
        $.post('/su', {un : un, pw: pw}, function(user){
            if(!user.error) me.local('un', un);
            res(!user.error);
        });
    });
};

//logout
LDB.prototype.logOut = function(){
    var me = this;
    return new Promise(function(res, rej){
        $.get('/lo', function(){
            me.clearLocal('un');
            res(true);
        });    
    });
};

//gets all the levels
LDB.prototype.getLevels = function(){
    var me = this;
    return new Promise(function(res, rej){
        $.get('/levels', function(arrayOfLevels){
            res(arrayOfLevels);
        });
    });
};


//sets or gets the level to play/edit
// eg. db.level() -> gets the current level to play/edit
// db.level({un:'user\'s name', name : 'name of level'}); //sets the level to choose for editing/playing
LDB.prototype.level = function(){
    if(arguments.length == 1){
        this.clearLocal('level');
        this.local('level', arguments[0]);
    }
    return this.local('level');
};

//gets game data for the level selected by play
LDB.prototype.loadLevel = function(){
    var me = this;
    return new Promise(function(res, rej){
        if(me.level() !== undefined)
            $.get('/playLevel', me.level(), res);
        else rej('error in selecting levels');
    });
};

//selects a level to edit: use loadEdit to get the data after this function returns
LDB.prototype.edit = function(level){
    return new Promise(function(res, rej){
        $.post('../editLevel', level, res);
    });
};

//selects a level to edit: use loadEdit to get the data after this function returns
LDB.prototype.editNew = function(){
    return new Promise(function(res, rej){
        $.post('../editNewLevel', res);
    });
};


//load the level which shall be edited
LDB.prototype.loadEdit = function(){
    var me = this;
    return new Promise(function(res, rej){
        if(me.level() !== undefined)
            $.get('/editLevel', me.level(), res);
        else $.get('/editLevel', res);
    });
};


//returns array of levels (does not include actual level data)
LDB.prototype.myLevels = function(){
    return new Promise(function(res, rej){
        $.get('../mylevs', res);
    });
};


//sets/gets local storage : use like db.local(key) -> returns value
//db.local(key, value) // sets localStorage[key] to value
LDB.prototype.local = function(x, y){
    if(arguments.length == 1 && window.localStorage[x] !== undefined){
        return JSON.parse(window.localStorage[x]);
    }else if(arguments.length == 2){
        window.localStorage[x] = JSON.stringify(y);
    }
    return undefined;
};

//db.clearLocal(key) removes key, db.clearLocal() removes all
LDB.prototype.clearLocal = function(x){
    if(arguments.length == 1){
        window.localStorage.removeItem(x);
    }else{
        window.localStorage.clear();
    }
};