
function Planet(args){
	if(args === undefined) args = {};
	var keys = Object.keys(args);
	var initialize = function(name,init){return keys.indexOf(name) == -1 ? init: args[name]; };

	this._shape 			= initialize('shape', Planet.randomShape(Math.round(3 + Math.random() * 3), Math.random()*40 + 20));
	this._position 			= initialize('position', new LV2(0,0));
	this._angle 			= initialize('angle', 0);
    this.color              = initialize('color', '#FF0000');
    this._worldPosition 	= this._position.copy();
	this._localRotator 		= [];
	this._worldTransformer 	= [];
	var animations 		= initialize('animations', []);
	for(var i = 0; i < animations.length; i++) this.addAnimation(animations[i]);
	this._updateShape();
}

Planet.prototype.toJSON = function(){
    var anims = [], i;
    for(i = 0; i < this._localRotator.length; i++) anims.push(this._localRotator[i].toJSON());
    for(i = 0; i < this._worldTransformer.length; i++) anims.push(this._worldTransformer[i].toJSON());
    var ret = {
        position : this._position.toJSON(),
        shape : this._shape.toJSON(),
        angle : this._angle,
        color : this.color,
        animations : anims
    };
    return ret;
};

Planet.fromJSON = function(ob){
    ob.position = LV2.fromJSON(ob.position);
    ob.shape = LPolygon.fromJSON(ob.shape);
    ob.animations = ob.animations.map(Lanimatronics.fromJSON);
    return new Planet(ob);
};

Planet.prototype.getShape = function(){
	return this._computedShape;
};

Planet.prototype.getAngle = function(){
	return this._angle;
};

Planet.prototype.getPosition = function(){
	return this._worldPosition.copy();
};


Planet.prototype.getTransform = function(){
	return LMat3.trans(this._worldPosition.x, this._worldPosition.y).mult(LMat3.rotate(this._angle));
};

Planet.prototype._updateShape = function(){
	this._computedShape = this._shape.transform(this.getTransform());
};

Planet.prototype.notifyAttach = function(){};
Planet.prototype.notifyDetach = function(){};
Planet.prototype.notifyStep   = function(){};


Planet._distanceFromLine = function(point, a, b){
	var normal = b.sub(a); normal.iunit(); normal.inormal();
	var test = point.sub(a);
	return Math.abs(test.dot(normal));
};

Planet._withinLineBounds = function(point, a, b){
	var ab = b.sub(a);
	var tp = point.sub(a);
	var distAB = ab.mag();
	var scalar = tp.projectionScalar(ab);
	if(scalar < 0 || scalar > distAB) return false;
	return true;
};

Planet._withinLineBoundsScalar = function(point, a, b){
	var ab = b.sub(a);
	var tp = point.sub(a);
	var distAB = ab.mag();
	var scalar = tp.projectionScalar(ab);
	return scalar / distAB;
};

Planet.prototype.getAttachmentInfo = function(point, normal){
	this._updateShape();
    var shape = this.getShape(), set = false, bestDistance = -1; //s
	var v1 = -1, v2 = -1;
	for(var i = 0; i < shape.points.length; i++){
		var j = (i+1) % shape.points.length;
		var a = shape.points[i], b = shape.points[j];
		if(Planet._withinLineBounds(point, a,b)){
			var distance = Planet._distanceFromLine(point, a, b);
			if(set && distance >= bestDistance) continue;
			set = true;
			bestDistance = distance;
			v1 = i, v2 = j;
		}
	}
	if(!set) throw -1;
	var retNorm = this._shape.points[v2].sub(this._shape.points[v1]);
	retNorm.iunit(); retNorm.inormal();
	var scalar = Planet._withinLineBoundsScalar(point, shape.points[v1], shape.points[v2]);
	var _a = this._shape.points[v1];
	var _b = this._shape.points[v2];
	var retPoint = _b.sub(_a);
	retPoint.iscale(scalar);
	retPoint.iadd(_a);
	return {
		newDirection : retNorm, //needed
		newPoint : retPoint, //needed
	};
};


Planet.prototype.step = function(time){
    if(this._localRotator.length > 0){ //rotation animator
		this._angle = this._localRotator[0].step(time, this._angle);
		if(this._localRotator[0].finished()){
			var r = this._localRotator.shift();
			if(r.alive()) this._localRotator.push(r);
			if(this._localRotator.length > 0) this._localRotator[0].reset();
		}
	}

	if(this._worldTransformer.length > 0){ 
		if(this._worldTransformer[0].finished()){
			var t = this._worldTransformer.shift();
			if(t.alive())
				this._worldTransformer.push(t);
			if(this._worldTransformer.length > 0) this._worldTransformer[0].reset(this._worldPosition);
		}
		if(this._worldTransformer.length > 0) this._worldPosition = this._worldTransformer[0].step(time);
		else this._position = this._worldPosition.copy();
		
	}else this._worldPosition = this._position;
	this._updateShape();
    this.notifyStep(time);
};

Planet.prototype._addAnimation = function(anim){
	if(anim._type == 'rotate')
		this._localRotator.push(anim);
	else this._worldTransformer.push(anim);
};

Planet.prototype.addAnimation = function(){
	for(var i = 0; i < arguments.length; i++)
		this._addAnimation(arguments[i]);
};

Planet.prototype.clearAnimations = function(){
	this._localRotator = [];
	this._worldTransformer = [];
};

Planet.randomShape = function(numSides, radius){
	var points = [];
	var previous = 0;
	for(var i = 0; i < numSides; i++){
		var randomNumber = Math.random()*0.6 + 0.4;
		points.push(randomNumber + previous);
		previous = randomNumber + previous;
	}
	var scalar = 360 / points[points.length-1];
	for(var i = 0; i < numSides; i++){
		points[i] = LV2.fromAngle(points[i] * scalar);
		points[i].iscale(radius);
	}
	var poly = new LPolygon(points);
	poly.reversePoints();
	return poly;
};


Planet.random = function(){
    var numSides = Math.round(3 + Math.random() * 3), radius = Math.random()*40 + 20;
	var points = [];
	var previous = 0;
	for(var i = 0; i < numSides; i++){
		var randomNumber = Math.random()*0.6 + 0.4;
		points.push(randomNumber + previous);
		previous = randomNumber + previous;
	}
	var scalar = 360 / points[points.length-1];
	for(var i = 0; i < numSides; i++){
		points[i] = LV2.fromAngle(points[i] * scalar);
		points[i].iscale(radius);
	}
	var poly = new LPolygon(points);
	poly.reversePoints();
	return poly;
};


Planet.circle = function(radius){
	var points = [];
	var previous = 0;
	var numSides = 30;
	for(var i = 0; i < numSides; i++){
		var angle = (i / numSides) * 360;
		points.push(LV2.fromAngle(angle).scale(radius));
	}
	var poly = new LPolygon(points);
	poly.reversePoints();
	return poly;
};

Planet.triangle = function(){
	var points = "169,701;843,701;506,116";
    points = points.split(';').map(function(x){ return LV2.fromString(x).scale(0.1).round(); });
	var poly = new LPolygon(points);
    poly.toOrigin();
    return poly;
};

Planet.hexigon = function(){
	var points = "36,743;338,1350;1106,1350;1451,743;1106,138;338,138";
    points = points.split(';').map(function(x){ return LV2.fromString(x).scale(0.1).round(); });
	var poly = new LPolygon(points);
    poly.toOrigin();
    return poly;
};


Planet.square = function(){
	var points = "0,0;50,0;50,-50;0,-50";
    points = points.split(';').map(function(x){ return LV2.fromString(x); });
	var poly = new LPolygon(points);
    poly.toOrigin();
    return poly;
};


Planet.octagon = function(){
	var points = "48,339;166,463;333,462;454,336;456,168;334,42;169,42;46,169";
    points = points.split(';').map(function(x){ return LV2.fromString(x).scale(0.1).round(); });
	var poly = new LPolygon(points);
    poly.toOrigin();
    return poly;
};