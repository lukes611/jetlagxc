
function Game(){
    this.state = 'pause';
    this.canvas = new LCanvas(360, 720);
    
    
   
    this.planets = [];
    this.planetConstructors = [];
    this.name = '-';
    this.by = 'no one';
    this.myAlertFunction = function(){};
    
    //player:
    var initialPosition = this.canvas.getCenter().add(new LV2(-50,-360));
    var me = this;
    this.ready = new Promise(function(res, rej){
        Player.default(initialPosition, me.canvas.w, 600).then(function(p){
            me.player = p;
            res();
        });
    });
}

Game.prototype.load = function(level){
    if(level.error) return false;
    this.name = level.name;
    this.by = level.un;
    this.planetConstructors = level.level.planets.map(function(planet){
        var _constructor = PlanetConstructor.fromJSON(planet);
        _constructor.compilePlanet();
        return _constructor;
    });
    this.reset();
};

Game.prototype.onAlert = function(cb){
    this.myAlertFunction = cb;
};


//game states: pause & play
Game.prototype.play = function(){
    this.state = 'play';
};

Game.prototype.pause = function(){
    this.state = 'pause';
};

Game.prototype.reset = function(){
    if(this.state == 'play') return;
    this.planets = this.planetConstructors.map(function(x){ x.compilePlanet(); return x.planet; });
    for(var i = 0; i < this.planets.length; i++){
        if(this.planets[i]._type == 'starter'){
            this.player.forceAttach(this.planets[i]);
            break;
        }
    }
};

Game.prototype.paint = function(){
    if(this.state == 'pause') return;
    var c = this.canvas;
    
    //clear screen
    c.resetTransform();
    c.setFill('black');
    c.rectf(0,0,c.w, c.h);
    
    var playerLocation = this.player.getPosition();
    
    c.context.translate(0, -playerLocation.y + c.h/2);
    
    if(this.player.image !== undefined){
        var base = this.player._base.scale(-1);
		c.context.save();
		c.context.translate(playerLocation.x, playerLocation.y);
		c.context.rotate(this.player.getAngle() / 57.3);
		c.context.translate(base.x, base.y);
		c.dimage(0, 0, this.player.image);
		c.context.restore();
	}
    
    for(var i = 0; i < this.planets.length; i++){
        this.paintPlanet(this.planets[i]);
    }
};

Game.prototype.paintPlanet = function(p){
    var t = p._type;
    if(t == 'stick' || t == 'starter' || t == 'ender'){
        this.canvas.setFill(p.color);
        this.canvas.polyf(p.getShape().points);
    }else if(t == 'death'){
        this.canvas.setStroke(p.color);
        this.canvas.lineWidth = 35;
        this.canvas.poly(p.getShape().points);
    }else if(t == 'timer'){
        this.canvas.setFill(p.color);
        this.canvas.polyf(p.getShape().points);
        var pos = p.getPosition();
        this.canvas.setFill('black');
        this.canvas.text(pos.x-10, pos.y+5, p.getTime())
    }
        
};

Game.prototype.alert = function(msg){
    console.log(msg);
    this.myAlertFunction(msg);
    this.pause();
};

Game.prototype.click = function(){
    this.player.launch();
};

Game.prototype.step = function(time){
    if(this.state == 'pause') return;
    this.player.attach(this.planets);
    for(var i = 0; i < this.planets.length; i++) this.planets[i].step(time);
    this.player.step(time);
	if(!this.player.inBounds()){
        this.alert('out of bounds');
    }
};