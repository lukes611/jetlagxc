/*
LC -> level creator has cont..:

button(parent, name, this, func)
input(parent, msg, name, this, func)
toggle(parent, msg, initialValue (true or false), this, func)
pointInput(parent, name, currentPoint, this, thisPlanet, funcInput[, funcClick])
colorInput(parent, msg, currentColor, this, func)
select(parent, name, options, default, this, func)

*/

//create a new button
LevelCreator.button = function(parent, name, thisObject, func){
    var button = $('<button>'+name+'</button>');
    button.click(func.bind(thisObject));
    parent.append(button);
    return button;
};

//create a new input section
LevelCreator.input = function(parent, msg, name, thisObject, func){
    var msgDiv = $('<div class="LevelCreatorInputDiv" ></div>');
    msgDiv.append($('<div>'+msg+'</div>'));
    var input = $('<input type="text" />');
    input.val(name);
    input.change(function(event){
        var data = input.val();
        func.bind(thisObject)(data);
    });
    msgDiv.append(input);
    parent.append(msgDiv);
    return [msgDiv, input];
};

//create a new toggle
LevelCreator.toggle = function(parent, msg, toggleOn, thisObject, func){
    var _ = $('<div class="LevelCreatorInputDiv"></div>');
    _.append($('<div>'+msg+'</div>'));
    var toggler = $('<input type="checkbox" />');
    toggler.prop('checked', toggleOn);
    _.append(toggler);
    toggler.change(function(event){
        var ret = toggler.prop('checked');
        func.bind(thisObject)(ret);
    });
    parent.append(_);
    return [_, toggler];
};


//pointInput must be fixed...
LevelCreator.pointInput = function(parent, name, currentPoint, thisObject, thisPlanet, funcInput, funcClick){
    if(funcClick === undefined) funcClick = funcInput;
    //root contains everything
    var root = $('<div class="LevelCreatorInputPoint" ></div>');
    root.append($('<div >'+name+': </div>'));
    //inp is the input
    var inp = $('<input type="text"></input>');
    inp.val(currentPoint.toString());
    inp.change(function(){
        var p = LV2.fromString(inp.val());
        funcInput.bind(thisObject)(p);
    });
    root.append(inp);
    //btn is the record button
    var btn = $('<button>record</button>');
    btn.click(function(){
        btn.css('background-color', 'red');
        thisPlanet.lc.addMouseListener({
            i : 1,
            f : function(pos){
                funcClick.bind(thisObject)(pos.round());
                thisPlanet.lc.updateGui();
            }
        });
    });
    root.append(btn);
    parent.append(root);
    return [root, inp, btn];
};

LevelCreator.colorInput = function(parent, msg, currentColor, thisObj, func){
    var div = $('<div class="LevelCreatorInputDiv"></div>');
    div.append($('<div>'+msg+'</div>'));
    var inp = $('<input type="color">');
    inp.val(currentColor);
    inp.change(function(){
        var c = inp.val();
        func.bind(thisObj)(c);
    });
    div.append(inp);
    parent.append(div);
    return [div, inp];
};

//click(point), move(point, previous, difference), end(point)
LevelCreator.prototype.initMouse = function(){
    var me = this;
    var helper = {
        start : new LV2(0,0),
        current : new LV2(0,0),
        pressed : false,
        correct : function(e){
            var p = new LV2(e.x, e.y);
            p.x /= Number($(me.canvas.canvas).css('width').split('').slice(0,-2).join(''));
            p.y /= Number($(me.canvas.canvas).css('height').split('').slice(0,-2).join(''));
            p.x *= me.canvas.w;
            p.y *= me.canvas.h;
            p.y += me.yFocus - me.canvas.h/2;
            return p;
        }
    };
    this.device.mouse(this.canvas.canvas, (function(event){
        var input = this.correct(event);
        me.debugPoint = input.copy();
        if(event.type == 'start'){
            this.start = input.copy();
            this.pressed = true;
            me.mouseClick(input.copy());
        }else if(event.type == 'end'){
            this.pressed = false;
            me.mouseEnd(input.copy());
        }else if(event.type == 'move'){
            if(this.pressed){
                this.current = input.copy();
                var dif = this.current.sub(this.start);
                me.mouseMove(input.copy(), this.start.copy(), dif);
                this.start = input.copy();
            }    
        }
    }).bind(helper));
};



LevelCreator.select = function(parent, name, options, def, thisObject, func) {
    var ret = $('<div class="LevelCreatorInputSel"></div>');
    ret.append($('<div>'+name+'</div>'));
    var sel = $('<select></select>');
    options.forEach(function(option){
        sel.append('<option>'+option+'</option>');
    });
    ret.append(sel);
    sel.val(def);
    func = func.bind(thisObject);
    sel.change(function(e){
        var input = sel.val();
        func(input);
    });
    parent.append(ret);
    return [ret, sel];
};

