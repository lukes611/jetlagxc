
var db = new LDB();
/*
    action : toggle, show or hide
    elems : an id name for a single element or class name for elements
    exclusions : an array of tagNames to exclude from the effect 
*/
function edisp(elems, action, exclusions){ //element-display function
    if(exclusions === undefined) exclusions = [];
    var isId = elems[0] == '#';
    if(isId)
        $(elems)[action]('slide');
    else{
        var elements = $(elems);
        for(var i = 0; i < elements.length; i++){
            var element = $(elements[i]);
            if(exclusions.indexOf(element.prop('tagName').toLowerCase()) == -1)
                element[action]('slide');
        }
    }
}

var menu = {
    on : false, //whether the menu is currently being displayed or not
    db : db, //the database to use
    getData : function(type){ //retrieves the form data
        var isSignUp = type == 'up';
        var unId = isSignUp ? '#unInputSU' : '#unInputSI';
        var pwId = isSignUp ? '#pwInputSU' : '#pwInputSI';
        return {
            un : $(unId).val(),
            pw : $(pwId).val()
        };
    },
    toggle : function(){ //toggles the display of the menu
        this.on = !this.on;
        var action = this.on ? 'show' : 'hide';
        edisp('#menuBar', action);
    },
    refresh : function(){ //refreshes the gui, after a log-in/out/sign-up has occured
        if(this.db.isLoggedIn()){
            edisp('.notLoggedInForm', 'hide');
            edisp('.loggedInForm', 'show')
            $('#menuBarLoginName').html('' + this.db.un());
        }else{
            edisp('.notLoggedInForm', 'show', ['form']);
            edisp('.loggedInForm', 'hide');
        }
    },
    viewForm : function(type){ //view a particular form : up[for sign-up] or in[for sign-in]
        var id = type == 'up' ? '#signUpForm' : '#signInForm';
        edisp(id, 'toggle');
    },
    postLogon : function(worked, errorDivId, errorMsg){
        var d = $(errorDivId);
        if(worked){
            d.hide();
            this.refresh();
        }else{
            d.html(errorMsg);
            edisp(errorDivId, 'show');
        }
    },
    signIn : function(){
        var input = this.getData('in');
        var me = this;
        this.db.logIn(input.un, input.pw).then(function(worked){
            me.postLogon(worked, '#signInErrorMessage', 'incorrect user-name or password');
        });
    },
    signUp : function(){
        var me = this;
        var input = this.getData('up');
        this.db.newAccount(input.un, input.pw).then(function(worked){
            me.postLogon(worked, '#signUpErrorMessage', 'user-name already exists');
        });
    },
    logOut : function(){
        this.db.logOut().then(this.refresh.bind(this));
    }






};

