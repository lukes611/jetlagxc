luke todo:

increase sizes
pause button
look at upside down bug
    
    
main screen:
    play
        list levels
            playLevel
                level has timer...
    custom levels
    
    
    
level-creator page:::    
    
directory hierarchy
jetlagxc:
    static:
        code
        html
        css
    server:
    start.js


	player:
		data:
			_position, _shape, _direction, _speed, [planet : start attachment], _attachmentInfo, _angle
		actions:
			*getShape() -> get the world shape
			*getAngle() -> get the angle
			*step(time) -> 
			*inBounds() -> returns whether the player is within bounds
			*attach(planetsList) -> attempts to attach the player to a list of planets,
			*canBeAttached() -> returns true if the player can be attached to other planets
			*getPosition() -> returns the position of the player
			*getDirection() -> returns the direction of the player
			*setDirection() -> sets _direction and updates _angle
			*launch() -> launches the player if possible
			*forceAttach(planet, edge1, edge2, terpValue) -> attaches the player to a particular planet
	Planet:
		data:
			_position, _angle, _worldPosition, _shape, _localRotator, _worldTransform
		actions:
			*getShape()
			*step(time)
			*getTransform()
			*getAttachmentInfo()
			*getPosition()
			*addAnimation(animation)
			*clearAnimations()
