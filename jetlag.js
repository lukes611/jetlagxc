var express = require('express');
var session = require('express-session');
var fs = require('fs');
var bodyParser = require('body-parser');


var JPDB = require('./server/JPDB');

var db = new JPDB();

process.stdin.resume();
process.on('exit',db.close.bind(db));
process.on('SIGINT', function(){
    process.exit(2);
});

console.log('creating server...');

console.log('setup: ', db.setup);

var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));
app.use(session({
    saveUninitialized : true,
    resave : true,
    secret : 'mysec123'
}));


setTimeout(function(){
    //db.newUser('thao', 'b', console.log);
    //db.getUser('luke', console.log);
    //db.auth('luke3', 'thaos611', console.log);
    //db.countLevels('luke',console.log);
    //db.getAllLevels(console.log);
    //db.getMyLevels('luke', console.log);
    //db.getMyLevel('luke', 'levelY', console.log);
}, 500);


//is logged-in?
app.get('/li', function(req, res){
    if(req.session.un)
        res.json({un : req.session.un, error : false});
    else res.json({error : true});
});

//actually log in
app.post('/li', function(req, res){
    db.auth(req.body.un, req.body.pw, function(worked){
        if(worked){
            req.session.un = req.body.un;
            res.json({un : req.body.un, error : false});
        }else res.json({error:true});
    });
});

app.get('/lo', function(req, res){
    delete req.session.un;
    res.json({error : false});
});

//sign up
app.post('/su', function(req, res){
    db.newUser(req.body.un, req.body.pw, function(ret){
        res.json(ret);
    });
});

//save a level
app.post('/saveLevel', function(req, res){
    var input = JSON.parse(req.body.level);
    if(req.session.un === undefined) res.json({error : true});
    else db.updateLevel(req.session.un, input, function(out){
        res.json(out);
    }); 
});

//get all levels
app.get('/levels', function(req, res){
    db.getAllLevels(function(levels){
        res.json(levels);
    });
});

//get my levels (names)
app.get('/mylevs', function(req, res){
    if(req.session.un === undefined) res.json([]);
    else{ 
        //console.log('getting levels for: ', req.session.un);
        db.getMyLevels(req.session.un, function(levels){
            res.json(levels);
        });
    }
});


//actually grab a playable object to play the level
app.get('/playLevel', function(req, res){
    db.getMyLevel(req.query.un, req.query.name, function(level){
       res.json(level); 
    });
});

//set the level to edit
app.post('/editLevel', function(req, res){
    req.session.edit = req.body.name;
    res.json({error : false});
});

//edit new level
app.post('/editNewLevel', function(req, res){
    if(req.session.edit !== undefined) delete req.session.edit;
    res.json({error : false});
});


//actually exit the level
app.get('/editLevel', function(req, res){
    if(!req.session.un || req.session.un != req.query.un) res.json({error : true});
    else{
        db.getMyLevel(req.query.un, req.query.name, function(ret){
            res.json(ret);
        });
    }
});


app.use(express.static('./static'));

app.listen(8142, function(){
    console.log('listening, ', arguments);
})
