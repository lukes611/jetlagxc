var mongodb = require('mongodb');


function JPDB(){
    this.isSetup = false;
    this._client = mongodb.MongoClient;
    this.url = 'mongodb://localhost:27017/jetplanetjs';
    this.open();
}

JPDB.prototype.close = function(){
    if(this.isSetup){
        this.db.close();
    }
    console.log('closing db...');
};

JPDB.prototype.open = function(){
    var me = this;
    this._client.connect(this.url, function(err, db){
        if(err){
            console.log('unable to log into db');
        }else{
            me.db = db;
            me.setup = true;
        }
    });
};


JPDB.prototype.newUser = function(un, pw, cb){
    var me = this;
    this.getUser(un, function(user){
        if(user.error){
    
            var Users = me.db.collection('users');
            Users.insert({un : un, pw:pw}, function(e, r){
                if(e) cb({error : true});
                else cb({error : false});
            });
            
        }else cb({error:true});
    });
    
};

JPDB.prototype.getUser = function(un, cb){
    var Users = this.db.collection('users');
    Users.findOne({un : un}, function(e, user){
        if(e || user == null) cb({error:true});
        else{
            user.error = false;
            cb(user);
        }
    });
};

JPDB.prototype.auth = function(un, pw, cb){
    this.getUser(un, function(user){
        if(user.error || user.pw != pw) cb(false);
        else cb(true);
    });
};

JPDB.prototype.updateLevel = function(un, level, cb){
    var Levels = this.db.collection('levels');
    Levels.update({un : un, name : level.name}, {un: un, name : level.name, level : level}, {upsert : true}, function(e){
        if(e) cb({error:true});
        else cb({error : false});
    });
};

//use like countLevels(cb) or countLevels(un, cb)
JPDB.prototype.countLevels = function(){
    var Levels = this.db.collection('levels');
    var args = arguments;
    var retF = function(e, count){
        if(e) args[args.length-1](0);
        else args[args.length-1](count);
    };
    
    if(args.length == 2) Levels.count({un : args[0]}, retF);
    else Levels.count(retF);
};



JPDB.prototype.getAllLevels = function(cb, includeLevel){
    var Levels = this.db.collection('levels');
    includeLevel = includeLevel === undefined ? false : includeLevel;
    var sel = includeLevel ? {_id:0} : {level : 0, _id:0};
    Levels.find({}, sel).toArray(function(e, d){
        if(e) cb([]);
        else cb(d);
    });
};

JPDB.prototype.getMyLevels = function(un, cb){
    var Levels = this.db.collection('levels');
    Levels.find({un : un}, {level:0, _id : 0}).toArray(function(e, d){
        if(e) cb([]);
        else cb(d);
    });
};

JPDB.prototype.getMyLevel = function(un, name, cb){
    var Levels = this.db.collection('levels');
    Levels.findOne({un : un, name : name}, function(e, level){
        if(e || level == null) cb({error:true});
        else{
            level.error = false;
            cb(level);
        }
    });
};


module.exports = JPDB;